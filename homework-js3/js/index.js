let num1 = +prompt("Введите первое число:");
let num2 = +prompt("Введите второе число:");
let operation = prompt("Введите математический оператор (+, -, *, /)");

function calc(num1, num2, operation) {
  switch (operation) {
    case "+":
      console.log(num1 + num2);
      break;
    case "-":
      console.log(num1 - num2);
      break;
    case "*":
      console.log(num1 * num2);
      break;
    case "/":
      console.log(num1 / num2);
      break;
  }
}
calc(num1, num2, operation);
