let userName, userAge;
do {
  userName = prompt("What is your name?");
  userAge = prompt("How old are you?");
} while (
  userName === "" ||
  isNaN(userAge) ||
  userAge < 0 ||
  userAge === "" ||
  userAge === null
);
if (+userAge >= 0 && +userAge <= 17) {
  alert("You are not allowed to visit this website");
} else if (userAge > 22) {
  alert(`Welcome, ${userName}`);
} else if (userAge >= 18 || userAge <= 22) {
  let question = confirm("Are you sure you want to continue?");
  if (question === true) {
    alert(`Welcome, ${userName}`);
  } else {
    alert("You are not allowed to visit this website");
  }
}
