"use strict"


    function MakeUser() {
        this.firstName = prompt('Введіть ваше імя');
        this.lastName = prompt('Введіть фамілію');
        this.birthday = prompt('Введіть дату народження', 'dd.mm.yyyy')

        if (!this.firstName || !this.lastName 
        || typeof this.firstName !== 'string' 
        || typeof this.lastName !== 'string' || this.firstName == +this.firstName 
        || this.lastName == +this.lastName || !this.birthday ||typeof this.birthday !== 'string') {
          console.error('Користувач не ввів потрібні данні');
        }
      }
      MakeUser.prototype.getAge  = function (){
        const date = new Date()
        const year = date.toLocaleDateString()
        return  year.slice(6, 10) - this.birthday.slice(6, 10)
      }
      MakeUser.prototype.getLogin = function () {
        const lowerFirstName = this.firstName.toLowerCase();
        const lowerLastName = this.lastName.toLowerCase();
    
        return this.firstName + ' ' + this.lastName + " = " + lowerFirstName.slice(0, 1) + lowerLastName;
      }
      MakeUser.prototype.getPassword = function() {
        const lowerLastName = this.lastName.toLowerCase();
        return this.firstName.slice(0, 1) + lowerLastName + this.birthday.slice(6, 10)
      }
    
      const newUser = new MakeUser();
      console.log(newUser.getPassword())
      console.log(newUser.getLogin());

      console.log('USER', newUser)
